
test

---

<!-- .slide: id="title-page" -->
# Test Driven Development

Stefan Hynek, Michelle Weidling<!-- .element: id="author-name" -->

<small>Niedersächsische Staats- und Universitätsbibliothek Göttingen</small><!-- .element: id="institution-name" -->

<small>https://gitlab.gwdg.de/mrodzis/slides</small><!-- .element: id="gitlab" -->

---

## Ablauf

- Einführung: Was ist Test Driven Development?
- Hands-on: Jupyter Notebook
- Fragen, Diskussion, Feedback

---

## Alltägliche Entwickler:innenprobleme

- Bugs
- unerwartete Seiteneffekte bei Codeänderungen
- ... die vielleicht erst viel später auffallen

---

## TDD

1. schreibe einen Test, der **fehlschlägt**
2. schreibe Code, der den Test gerade so **bestehen** lässt
3. refaktoriere deinen Code
4. repeat

---

Zeit fürs Jupyter Notebook!

---

## TDD und andere Entwicklungsmodelle

- Documentation Driven Development
- Behaviour Driven Development
- Issue Driven Development
- ...

... schließen sich nicht aus!

---

## Vorteile

- erzeugt kleine, gut testbare Funktionen mit hoher Kohäsion und geringer Kopplung
  - erzwingt ein halbwegs sauberes Moduldesign
- erleichtert spätere Änderungen (_fearless programming_)
  - auch zeitliche Einsparungen
- Regressionstests inklusive

---

## Nachteile

- Änderungen an Datenstrukturen bringen z.T. umfangreiche Änderungen an den Tests mit sich
- Code immer nur so gut wie die Tests (_boundary cases_)
  - ggf. falsche Sicherheit ("da kann nichts falsch sein, die Tests laufen ja durch!")

---

## TDD einsetzen – ja/nein?

- Tests sind (i.d.R.) besser als keine Tests
- niedrigschwellige Lösung: **TDD beim Debugging**

---

## Neugierig geworden?

- Beck, Kent. Test-Driven Development By Example. Boston, London 2003.
- Fowler, Martin. Refactoring. Improving the Design of Existing Code. Boston, London 2019.
