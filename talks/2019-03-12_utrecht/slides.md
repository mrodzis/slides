### EURISE Network
### Technical Reference

  Michelle Weidling

  <small>Göttingen State and University Library</small>

  <small>[weidling@sub.uni-goettingen.de](weidling@sub.uni-goettingen.de)</small>

  <small>[https://mrodzis.pages.gwdg.de/slides/talks/2019-03-12_utrecht](https://mrodzis.pages.gwdg.de/slides/talks/2019-03-12_utrecht)</small>

  <small>[https://eurise-network.github.io/technical-reference/](https://eurise-network.github.io/technical-reference/)</small>

--

## Outline

- What?
- Why?
- Content?
- How to implement it?

--

## What is it?

General **guidelines** for improving software quality (SQ)
throughout the software life cycle

--

## The TR covers...

- development
- operation
- policy recommendations
- software quality checklist

--

## Blueprint

- is meant to be customized:
  - choose whatever fits you project/institution

--

  <!-- .slide: data-background-color="lightgreen" -->
## Example

  "We'd rather use SVN instead of git."

  version control ✓

--

## Status and sources

- "living document", still in discussion
- literature, e.g. "Clean Code" (R. Martin)
- other technical references, e.g. German Aerospace Center (DLR)
- day-to-day work, personal experience, known best practices, ...

--

### What we want for our software

- should be maintainable
- should be actually used

⇝ high quality makes this possible!

--

## But why guidelines?

--

<!-- .slide: data-background-color="lightgreen" -->
## Examples

"I don't have time for writing tests."

"My code doesn't need documentation."

--

## But why guidelines?

- software quality 🗲 priority
- software quality 🗲 day-to-day work
- wrong expectations/predictions

⇝ help us to focus on the important aspects

--


## Parts of the Reference


- development
- operation
- policy recommendations
- software quality checklist

--

### Development

- README
- documentation
- usability & accessibility
- CHANGELOG
- ...

--

<!-- .slide: data-background-color="lightgreen" -->
## Examples

"Use version control. Right from the start."

"Keep a CHANGELOG."

--

### Operation

- infrastructure docs
- server docs
- security
- incidents and postmortems

--

<!-- .slide: data-background-color="lightgreen" -->
## Examples

"Document the system your software <br/>needs to run properly."

"Monitor this system."

--
### Policies

- code hosting policies
- release policy

--

<!-- .slide: data-background-color="lightgreen" -->
## Examples

"Apply Semantic Versioning to your software.<br/>(Pro tip: Keeping a CHANGELOG will help you.)"

"Develop a road map for the features to come."

--
### SQ Checklist

Questions you should ask yourself. Iteratively.

--

<!-- .slide: data-background-color="lightgreen" -->
## Examples

"Does the software fit all its requirements?"

"Is the documentation up to date and accessible?"

--
## Isn't that unrealistic?

Probably. But you should try. <br/>It saves money on the long run.<!-- .element: class="fragment fade-in" -->

--
## Reality check

Not every recommendation may suit your project/institution.

--
## Adjust your focus

Software is part of research, too!

--

<!-- .slide: data-background-color="lightgreen" -->
### Little things you can do

- Ask a colleague to do a short usability test (5mins!). <!-- .element: class="fragment fade-in" -->
- Dedicate 1h/week to write/update docs. <!-- .element: class="fragment fade-in" -->
- **Use the TR from the start when beginning a new project.** <!-- .element: class="fragment fade-in" -->

--

### tl;dr

- **customizable** guidlelines for software quality
- a little work invested goes a long way

--

Thank you for your attention!

--

Questions?
