<!-- .slide: id="title-page" -->

# EMo-Viewer

Michelle Weidling<!-- .element: id="author-name" -->

<small>Niedersächsische Staats- und Universitätsbibliothek Göttingen</small><!-- .element: id="institution-name" -->

<small>https://gitlab.gwdg.de/mrodzis/slides</small><!-- .element: id="gitlab" -->

<small>Editionsprojektetreffen, 05.08.2020</small><!-- .element: id="date" -->

<div class="wide">
![Uni Göttingen/SUB](img/goettingen.png)<!-- .element: id="goettingen-img" -->
</div>

---

## Übersicht

- Was ist der EMo-Viewer?
- Was kann er?
- Wie wird er entwickelt?
- Was soll er noch können?
- Wie kann ihn ihn nutzen?

---

## Was ist der EMo-Viewer?

- EMo = "**E**ditions**Mo**dule"
- **Idee:**
  - generisches Front End, um Editionstexte und dazugehörige Bilder anzuzeigen
  - ... [so](https://subugoe.pages.gwdg.de/emo/Qviewer/develop/#/) sieht er aktuell aus
  - nicht tausend Interaktionsfeatures für User\*innen, sondern gute und solide Anzeige von edierten Texten und Faksimiles
  - ist komplett agnostisch gegenüber den Forschungsdaten → greift auf API endpoint zu (*dazu später mehr*)

---

## Was kann er?

- Darstellung von
  - Texten
  - Bildern
  - Informationen zu den Metadaten der Forschungsdaten
- Navigation
- Mehrsprachigkeit
- *embedded* vs. *standalone*

---

## Wie wird er entwickelt?

- im Rahmen des [Ahikar-Projekts](https://www.sub.uni-goettingen.de/projekte-forschung/projektdetails/projekt/ahiqar/)
- gebaut mit [Quasar Framework](https://quasar.dev/), das auf [Vue.js](https://vuejs.org/) basiert

---

## Was soll er noch können?

- **aktuell:**
  - Implementation von Annotationen, d.h. Zusatzinformationen zum Text
    - *"Hebe mir alle Personennamen auf der Seite hervor."*
    - *"Zeig mir den editorischen Kommentar zu folgender Stelle an."*
  - Konfigurierbarkeit für projektspezifische Designanforderungen
- **in weiterer Zukunft:** was auch immer uns noch einfällt und sinnvoll erscheint

---

## Wie kann ihn ihn nutzen?<small>#1</small>

- [Repo](https://gitlab.gwdg.de/subugoe/emo/Qviewer) clonen bzw. Release nutzen, um den Viewer ggf. anzupassen
- EMo-Viewer auf einem Server laufen lassen (oder [GitLab pages](https://gitlab.gwdg.de/subugoe/emo/Qviewer/-/blob/develop/.gitlab-ci.yml#L20) nutzen)

---

## Wie kann ihn ihn nutzen?<small>#2</small>

- **API endpoints** einrichten:
  - Back End muss eine API bereitstellen, die zur [generischen TextAPI](https://subugoe.pages.gwdg.de/emo/text-api/) konform ist
    - angelehnt an die [IIIF Image API](https://iiif.io/api/image/3.0/)
    - unterteilt Ressourcen in Collections, Manifests und Items
    - [Beispiel Ahikar](https://gitlab.gwdg.de/subugoe/ahiqar/api-documentation) (Zugriff auf Anfrage gern bei mir, da noch kein offizielles Release)

---

## Wie kann ihn ihn nutzen?<small>#3</small>

- eigene Viewer-Instanz mit entsprechendem API-Einstiegspunkt [verbinden](https://gitlab.gwdg.de/subugoe/emo/Qviewer#connecting-the-viewer-to-a-backend)
- ... fertig!
- bei Fragen/Problemen zur Nutzung gern an mich wenden

---

<!-- .slide: id="thank-note" -->
## Fragen? Anregungen? Danke!