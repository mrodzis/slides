<!-- .slide: id="title-page" -->
![Titel](img/title.png)<!-- .element: id="title-img" -->
# KONDA
Michelle Weidling<!-- .element: id="author-name" -->

<small>Niedersächsische Staats- und Universitätsbibliothek Göttingen</small><!-- .element: id="institution-name" -->

<small>https://gitlab.gwdg.de/mrodzis/slides</small><!-- .element: id="gitlab" -->

<small>AG Sammlungsdigitalisierung, 14.10.2019</small><!-- .element: id="date" -->

<div class="wide">![DDK](img/ddk.png)<!-- .element: id="ddk-img" -->
![FMI](img/fmi.jpg)<!-- .element: id="fmi-img" -->
![Uni Marburg](img/marburg.jpg)<!-- .element: id="marburg-img" -->
![Uni Göttingen/SUB](img/goettingen.png)<!-- .element: id="goettingen-img" -->
![BMBF](img/bmbf.png)<!-- .element: id="bmbf-img" --></div>

---

## TOC:

* Was ist KONDA?
* Wer ist beteiligt?
* Was hat das mit der AG Sammlungsdigitalisierung zu tun?

---

**KONDA**:

<block>**KON**tinuierliches Qualitätsmanagement von dynamischen Forschungs**DA**ten zu Objekten der materiellen Kultur unter Nutzung des LIDO-Standards</block>

---

## Themen:

- Kontinuierliche **Qualitätssicherung**, generisch und spezifisch (Kunst-/Kulturgeschichte)
- **Vagheit** in Forschungsdaten
  - maschinenlesbar (und standardisiert) darstellen
  - *Beispiel:* Datierung eines Werks

---

## Themen:

- **Unsicherheiten** in Forschungsdaten
  - klassifizieren und kategorisieren
  - maschinenlesbar (und standardisiert) darstellen
  - *Beispiel:* die Zuordnung eines Künstlers zu einem Werk

---

## Themen:

- **Ungenauigkeiten** in Forschungsdaten
  - *Beispiel:* Geburtsdatum einer historischen Person vs. Taufdatum
- **Dynamik** in Forschungsdaten
  - *Beispiel:* neue Erkenntnisse durch neue technologische Möglichkeiten

---

## Was wir erarbeiten:

- Entwicklung **generischer** Qualitätsmanagement-Prozesse
  - werden dann auf Daten zu _Objekten materieller Kultur_ angewendet

---

## Was wir erarbeiten:

- **Standardisierung** und **Qualitätsverbesserung** bei Transformation und Publikation von Daten
  - Weiterentwicklung des _LIDO_-Standards mit der Fach-Community
  - Herausgabe von _Handbüchern_ und Empfehlungen zu _good practices_

---

## Wer wir sind:

- Deutsches Dokumentationszentrum für Kunstgeschichte – Bildarchiv Foto Marburg<!-- .element: class="loose" -->
- Fachbereich Mathematik und Informatik der Philipps-Universität Marburg<!-- .element: class="loose" -->
- Niedersächsische Staats- und Universitätsbibliothek Göttingen<!-- .element: class="loose" -->

---

## KONDA in Göttingen:

- KONDA arbeitet eng mit der **Zentralen Kustodie** zusammen
  - *Anforderungserfassung* zum Qualitätsmanagement
  - generischer *QM-Prozess* wird auf Daten der Universitätssammlungen (Auswahl) angewendet
  - erarbeitete *good practices* fließen in Zentrale Kustodie zurück

---

## KONDA in Göttingen:
- KONDA und die AG Sammlungsdigitalisierung:
  - *Kurationskriterien* und
  - *Qualitätsmanagement* (Erschließung/Datenpublikation) werden aufgenommen

---


<!-- .slide: id="thank-note" -->
## Vielen Dank für Ihre Aufmerksamkeit!
