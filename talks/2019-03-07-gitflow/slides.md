## Besser im Team <br/> entwickeln mit *git flow*

<small>Am Beispiel von SADE</small>

---

### Was ist und zu welchem Ende<br/> studieret man git flow?

- Branchingmodell für git
- 💡: semantisch getrennte branches
- [🔗](https://nvie.com/posts/a-successful-git-branching-model/) Vincent Driessen: “A successful Git branching model”

--

![sketch](img/gitflow.svg) <!-- element: width="800" -->

<small>https://wac-cdn.atlassian.com/dam/jcr:61ccc620-5249-4338-be66-94d563f2843c/05%20(2).svg?cdnVersion=lc</small>

--

<!-- .slide: data-background-iframe="https://danielkummer.github.io/git-flow-cheatsheet/" -->

---

## Warum der Aufwand?

Modularer Aufbau ermöglicht...

- weniger Merge-Konflikte
- übersichtliche Repo-History
- Code Reviews
- Semantic versioning/CHANGELOG

---

<!-- .slide: data-background-color="#ecf9f2" -->

## git flow in SADE

Issues (gesammelt in GitLab) <!-- .element: class="fragment fade-in-then-semi-out" -->

→ Features/Bugfixes  <!-- .element: class="fragment fade-in" -->

--

<!-- .slide: data-background-color="#ecf9f2" -->

## git flow in SADE

```bash
git flow feature start "#01-short-issue-description"
```
… Arbeiten am Feature <!-- .element: class="fragment fade-in" -->


--

<!-- .slide: data-background-color="#ecf9f2" -->

## git flow in SADE

1. Merge Request an andere\*n Entwickler\*in <!-- .element: class="fragment fade-in-then-semi-out" -->
1. Code Review <!-- .element: class="fragment fade-in-then-semi-out" -->
1. Merge <!-- .element: class="fragment fade-in-then-semi-out" -->
1. Glücklich sein! <!-- .element: class="fragment fade-in" style="color: #4cb34c;" -->

--

## release

* *branch protection*, ein Feature von GitLab, muss deaktivert werden
* release = merge to `master`
* `master` bleibt sonst unangetastet
* source kann nur `develop` oder `hotfix/#00-desc` sein

--

### relase und CI

der CI ist entsprechend einzustellen

```yaml
build-master:
  only:
      - master
  stage: build
  script:
    - cp master.build.properties local.build.properties
    - npm install
    - ant test
  artifacts:
    paths:
      - build/*.xar
      - test/
```

--

### relase und CI

entsprechend bekommen alle andere jobs:
```yaml
build-develop:
  except:
      - master
      - tags
```

---

## git flow hooks

--

“You may remember me from such great inventions as [*git hooks*](https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks)”

<small>Troy McClure</small>

--

### settings

bei `git flow init` einstellen

der kann entweder zentral sein, oder im Projekt liegen (SADE).

Dateinamen (wie bekannt):
* `(pre|post|filter)`
* `-git-flow`
* `-(feature|bugfix|hotfix)`
* `-(start|finish|publish)`

z.Bsp.: `pre-git-flow-feature-start`

--

Einsatz bei [SADE](https://gitlab.gwdg.de/SADE/SADE/blob/4b5b1bb4f00451c13559bb13814a5eb877d87c0c/.hooks/pre-flow-feature-start)


---

Fragen?

---

Danke!
