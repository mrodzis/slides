<!-- .slide: id="title-page" -->
![Titel](img/title.png)<!-- .element: id="title-img" -->
# LIDO und KONDA
Markus Matoni, Regine Stein, Michelle Weidling<!-- .element: id="author-name" -->

<small>Niedersächsische Staats- und Universitätsbibliothek Göttingen</small><!-- .element: id="institution-name" -->

<small>https://gitlab.gwdg.de/mrodzis/slides</small><!-- .element: id="gitlab" -->

<small>Herbsttagung Berlin, 21.10.2019</small><!-- .element: id="date" -->

<div class="wide">![DDK](img/ddk.png)<!-- .element: id="ddk-img" -->
![FMI](img/fmi.jpg)<!-- .element: id="fmi-img" -->
![Uni Marburg](img/marburg.jpg)<!-- .element: id="marburg-img" -->
![Uni Göttingen/SUB](img/goettingen.png)<!-- .element: id="goettingen-img" -->
![BMBF](img/bmbf.png)<!-- .element: id="bmbf-img" --></div>

---

## Stand LIDO

* Draft **v1.1** in Arbeit
  * noch finale Klärung einzelner Erweiterungswünsche
  * Release noch 2019 geplant
  * neue **Publikationsstrecke** wird erarbeitet
    * automatisches Erzeugen der Dokumentation aus dem Schema

---

## LIDO und KONDA

* generischen **Qualitätsmanagementprozess** auf LIDO anwenden
* **Kurationskriterien** auf Basis des allgemeinen LIDO-Standards sowie für spezifische Gattungen erarbeiten

---

## LIDO und KONDA

* **Weiterentwicklung** von LIDO
  * insb. in Hinblick auf *Dynamik* und *Unsicherheiten*
* Umsetzung des Schemas in CIDOC-CRM-basierte **RDF-Repräsentationen**
  * Beitrag zu *LOD*

---

## LIDO und KONDA

* ... und **Dokumentation** (Handbücher, <i>good practices</i>, ...)

<block>→ alles in enger Zusammenarbeit mit der LIDO-AG und der Community</block>

---

## Warum wir hier sind:

<center>Erfassung des **status quo** und **Anforderungserfassung**:
<br />
Was läuft gut? Was läuft schlecht?</center>
