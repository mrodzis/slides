<!-- .slide: id="title-page" -->
![Titel](img/title.png)<!-- .element: id="title-img" -->
# KONDA
Michelle Weidling<!-- .element: id="author-name" -->

<small>Niedersächsische Staats- und Universitätsbibliothek Göttingen</small><!-- .element: id="institution-name" -->

<small>https://gitlab.gwdg.de/mrodzis/slides</small><!-- .element: id="gitlab" -->

<small>KONDA Community Workshop, Berlin, 24.10.2019</small><!-- .element: id="date" -->

<div class="wide">![DDK](img/ddk.png)<!-- .element: id="ddk-img" -->
![FMI](img/fmi.jpg)<!-- .element: id="fmi-img" -->
![Uni Marburg](img/marburg.jpg)<!-- .element: id="marburg-img" -->
![Uni Göttingen/SUB](img/goettingen.png)<!-- .element: id="goettingen-img" -->
![BMBF](img/bmbf.png)<!-- .element: id="bmbf-img" --></wide>

---

![rembrandt](img/rembrandt.png)<!-- .element: id="rembrandt-img" -->
<small>Rembrandt (Umkreis), Mann mit dem Goldhelm, 1985 abgeschrieben</small>

---

**KONDA**:

<block>**KON**tinuierliches Qualitätsmanagement von dynamischen Forschungs**DA**ten zu Objekten der materiellen Kultur unter Nutzung des LIDO-Standards</block>

---

## Worum es geht:

- Kontinuierliche, systematische **Qualitätssicherung**, generisch und spezifisch
  - scope: _Objekte_ der materiellen Kultur
  - ein zentrales Thema: _Architektur_
  - Hintergrund: _Heterogenität_

---

## Worum es geht:

- **Vagheit** in Forschungsdaten
  - z.B. Datierung eines Werks
- **Ungenauigkeiten** in Forschungsdaten
  - z.B. Geburtsdatum einer historischen Person vs. Taufdatum

---

## Worum es geht:

- **Dynamik** in Forschungsdaten
  - Veränderung von Forschungsfragen, Methoden und Technologien

→ sollte sich in den Forschungsdaten _explizit_ und _strukturiert_ widerspiegeln

---


  1290 (?)

  1290?

  vermutlich 1290

  1290 (ca.)

  circa 1290

---

## Worum es geht:

- bisher nur wenig **Standardisierung**
  - Qualitätssicherung von Forschungsdaten zu Objekten der materiellen Kultur
  - Modellierung von dynamischen, z.T. unsicheren Daten

---

## Ziele:

- Systematische **Qualitätssicherung**, kontinuierlich über den **gesamten Lebenszyklus** von Daten
  - Erbarbeitung eines generischen Qualitätsmanagementprozesses
  - Besonderes Augenmerk auf _unsichere_ und _ungenaue_ Daten

---

## Ziele

- **Qualitätssicherung** auf **LIDO** anwenden
  - _Weiterentwicklung_ des LIDO-Standards mit der Fach-Community
  - Internationale und interdisziplinäre _Zusammenarbeit_ (Workshops, Engagement in Arbeitsgruppen, z.B. Deutscher Museumsbund, ICOM, …)

---

## Ziele:

- Herausgabe von **Handbüchern** und **good practices**


→ Verbesserung der **Qualität von Forschungsdaten** zu Objekten der materiellen Kultur

---

## Eckdaten

- BMBF-Förderung seit Juli 2019
- Kontext:
  - Förderung von Forschungsvorhaben zur Entwicklung und Erprobung von Kurationskriterien und Qualitätsstandards von Forschungsdaten im Zuge des digitalen Wandels im deutschen Wissenschaftssystem

---

## Wer wir sind:

- Deutsches Dokumentationszentrum für Kunstgeschichte – Bildarchiv Foto Marburg
  - Dr. Christian Bracht
- Fachbereich Mathematik und Informatik der Philipps-Universität Marburg
  - Prof. Dr. Gabriele Taentzer
- Niedersächsische Staats- und Universitätsbibliothek Göttingen
  - Regine Stein

---

## Warum Sie heute hier sind:

- Erfassung
  - des status quo
  - von Anforderungen an die Qualitätssicherung

  → Ihre Erfahrung ist unser Input!

---

<!-- .slide: id="thank-note" -->
## Vielen Dank für Ihre Aufmerksamkeit!
