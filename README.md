# Michelle Weidling's talks

The setup for these talks is powered by Mathias Göbel's [Markdown2reveal slides parser](https://gitlab.gwdg.de/mgoebel/slides).

General URL syntax: https://mrodzis.pages.gwdg.de/slides/talks/name-of-talk/#

## Talks

* [About Git Flow (07.03.2019, RDD)](https://mrodzis.pages.gwdg.de/slides/talks/2019-03-07-gitflow)
* [EURISE Network Technical Reference (12.03.2019, Utrecht)](https://mrodzis.pages.gwdg.de/slides/talks/2019-03-12_utrecht)
* [About KONDA (14.10.2019, AG Sammlungsdigitalisierung)](https://mrodzis.pages.gwdg.de/slides/talks/2019-10-14-sammlungsdigitalisierung)
* [LIDO und KONDA (21.10.2019, Herbsttagung)](https://mrodzis.pages.gwdg.de/slides/talks/2019-10-21-konda-lido)
* [About KONDA (23.10.2019, Herbsttagung)](https://mrodzis.pages.gwdg.de/slides/talks/2019-10-23-konda-herbsttagung)
* [About KONDA (24.10.2019, Herbsttagung)](https://mrodzis.pages.gwdg.de/slides/talks/2019-10-24-konda-cw)
* [LIDO und KONDA (25.10.2019, Community-Workshop I)](https://mrodzis.pages.gwdg.de/slides/talks/2019-10-25-konda-lido)
* [EMo-Viewer (05.08.2020, Editionsprojektetreffen)](https://mrodzis.pages.gwdg.de/slides/talks/2020-08-05-emo)
* [Test Driven Development (15.12.2020, TFSQ)](https://mrodzis.pages.gwdg.de/slides/talks/2020-12-15-workshop-tdd)

## Credits

[Reveal.js](https://github.com/hakimel/reveal.js)
[Mathias Göbel](https://gitlab.gwdg.de/mgoebel)
